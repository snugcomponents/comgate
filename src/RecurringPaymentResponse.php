<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate;

/**
 * @property-read RecurringPayment $recurringPayment
 * @property-read ?string $transId
 */
class RecurringPaymentResponse
{
    public function __construct(
        private RecurringPayment $recurringPayment,
        private ?string $transId,
    ) { }
    
    public static function create(
        RecurringPayment $recurringPayment,
        ?string $transId,
    ): static {
        return new static (
            $recurringPayment,
            $transId,
        );
    }

	/****************************** GETTERS ******************************m*b*/

	public function getRecurringPayment(): ?RecurringPayment { return $this->recurringPayment; }
	public function getTransId(): ?string { return $this->transId; }
}
