<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate;

use Snugcomponents\Comgate\Helpers\Validator\PaymentValidator;
use Snugcomponents\Comgate\Providers\AllowedMethodsProviderFactory;

/**
 * Class Payment for details go for Comgate documentation
 * @see https://help.comgate.cz/docs/en/protocol-api-en#creating-a-payment
 * @see https://help.comgate.cz/docs/api-protokol#zalo%C5%BEen%C3%AD-platby
 *
 * @property-read string $method
 * @property-read ?string $lang
 * @property-read ?bool $preauth
 * @property-read ?bool $verification
 * @property-read ?string|false $embedded
 * @property-read bool $initRecurring
 * @property-read ?string $applePayPayload
 */
final class Payment extends BasePayment
{
    public function __construct(
        AllowedMethodsProviderFactory $allowedMethodsProviderFactory,
		int $price,
		string $curr,
		string $label,
		string $refId,
		string $email,
		bool $prepareOnly,
        protected string $method,
        protected bool $initRecurring,

		?bool $test = null,
		?string $country = null,
		?string $account = null,
		?string $phone = null,
		?string $name = null,
		?bool $eetReport = null,
		null|array|string $eetData = null,

        protected ?bool $preauth = null,
        protected ?string $lang = null,
        protected ?bool $verification = null,
        protected string|false $embedded = false,
        protected ?string $applePayPayload = null,
	) {
        parent::__construct(
            $price,
            $curr,
            $label,
            $refId,
            $email,
            $prepareOnly,
            $test,
            $country,
            $account,
            $phone,
            $name,
            $eetReport,
            $eetData,
        );

        PaymentValidator::isLangValid($this->lang);
        PaymentValidator::isMethodValid(
            method: $this->method,
            allowedMethodsProvider: $allowedMethodsProviderFactory->create('cs', $curr, $country),
        );
    }

	/****************************** GETTERS ******************************a*j*/

    public function getMethod(): string { return $this->method; }
    public function getPreauth(): ?bool { return $this->preauth; }
	public function getInitRecurring(): bool { return $this->initRecurring; }
    public function getLang(): ?string { return $this->lang; }
    public function getVerification(): ?bool { return $this->verification; }
    public function getEmbedded(): string|false { return $this->embedded; }
	public function getApplePayPayload(): ?string { return $this->applePayPayload; }
}
