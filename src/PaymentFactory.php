<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate;

interface PaymentFactory
{
    public function create(
        int $price,
        string $curr,
        string $label,
        string $refId,
        string $email,
        bool $prepareOnly,
        string $method,
        bool $initRecurring,

        ?bool $test = null,
        ?string $country = null,
        ?string $account = null,
        ?string $phone = null,
        ?string $name = null,
        ?bool $eetReport = null,
        null|array|string $eetData = null,

        ?bool $preauth = null,
        ?string $lang = null,
        ?bool $verification = null,
        string|false $embedded = false,
        ?string $applePayPayload = null,
    ): Payment;
}
