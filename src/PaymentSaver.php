<?php declare(strict_types = 1);

namespace Snugcomponents\Comgate;

use Nette\SmartObject;
use Snugcomponents\Comgate\Helpers\ComgatePaymentConfirm;
use Snugcomponents\Comgate\Providers\PaymentConfirmRequestDataProviderFactory;

class PaymentSaver
{
    use SmartObject;
    
    public function __construct(
        private PaymentConfirmRequestDataProviderFactory $paymentConfirmRequestDataProviderFactory,
        private ComgatePaymentConfirm $comgatePaymentConfirm,
    ) { }

	public function confirm(): void
	{
		$this->comgatePaymentConfirm->confirm($this->paymentConfirmRequestDataProviderFactory->create());
    }
}
