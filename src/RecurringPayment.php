<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate;

/**
 * Class Payment for details go for Comgate documentation
 * @see https://help.comgate.cz/docs/en/protocol-api-en#creating-a-payment
 * @see https://help.comgate.cz/docs/api-protokol#zalo%C5%BEen%C3%AD-platby
 *
 * @property-read string $initRecurringId
 */
final class RecurringPayment extends BasePayment
{
    public function __construct(
        int $price,
        string $curr,
        string $label,
        string $refId,
        string $email,
        bool $prepareOnly,

        protected string $initRecurringId,

        ?bool $test = null,
        ?string $country = null,
        ?string $account = null,
        ?string $phone = null,
        ?string $name = null,
        ?bool $eetReport = null,
        null|array|string $eetData = null,
	) {
        parent::__construct(
            $price,
            $curr,
            $label,
            $refId,
            $email,
            $prepareOnly,
            $test,
            $country,
            $account,
            $phone,
            $name,
            $eetReport,
            $eetData,
        );
    }

	/****************************** GETTERS ******************************a*j*/

	public function getInitRecurringId(): string { return $this->initRecurringId; }
}
