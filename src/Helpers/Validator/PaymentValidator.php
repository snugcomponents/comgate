<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate\Helpers\Validator;

use Nette\StaticClass;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use Nette\Utils\Strings;
use Nette\Utils\Validators;
use Snugcomponents\Utils\Validators as SnugValidators;
use Snugcomponents\Comgate\Exceptions\ValidationException;
use Snugcomponents\Comgate\Providers\AllowedMethodsProvider;

class PaymentValidator
{
    use StaticClass;
    
    private const 
        DEFAULT_COUNTRY = 'CZ',
        DEFAULT_LANG = 'cs',
        DEFAULT_METHOD_DESCRIPTION_LANG = 'cs',
        MAX_LABEL_LENGHT = 16,
        BASE64_REGEX = '~^(?:[A-Za-z\d+/]{4})*(?:[A-Za-z\d+/]{3}=|[A-Za-z\d+/]{2}==)?$~',
        VALIDATE_METHOD_REGEX_BASE = '~^(((%s))( *(\+|-) *))*(?2)$~';

    private static array $countries = [
        'ALL' => 'ALL',
        'AT' => 'AT', 
        'BE' => 'BE',
        'CY' => 'CY',
        'CZ' => 'CZ',
        'DE' => 'DE', 
        'EE' => 'EE',
        'EL' => 'EL',
        'ES' => 'ES',
        'FI' => 'FI',
        'FR' => 'FR',
        'GB' => 'GB',
        'HR' => 'HR',
        'HU' => 'HU',
        'IE' => 'IE',
        'IT' => 'IT',
        'LT' => 'LT',
        'LU' => 'LU',
        'LV' => 'LV',
        'MT' => 'MT',
        'NL' => 'NL',
        'NO' => 'NO',
        'PL' => 'PL',
        'PT' => 'PT', 
        'RO' => 'RO',
        'SK' => 'SK',
        'SL' => 'SL',
        'SV' => 'SV',
        'US' => 'US',
    ];
    private static array $currencies = [
        'CZK' => 'CZK', 
        'EUR' => 'EUR', 
        'PLN' => 'PLN', 
        'HUF' => 'HUF', 
        'USD' => 'USD', 
        'GBP' => 'GBP', 
        'RON' => 'RON', 
        'HRK' => 'HRK', 
        'NOK' => 'NOK', 
        'SEK' => 'SEK', 
    ];
    private static array $minPrices = ['CZK' => 1, 'EUR' => 0.1, 'PLN' => 1, 'HUF' => 100, 'USD' => 1, 'GBP' => 1, 'RON' => 5, 'HRK' => 1, 'NOK' => 0.5, 'SEK' => 0.5];
    private static array $langs = [
        'cs' => 'cs', 
        'sk' => 'sk', 
        'en' => 'en', 
        'pl' => 'pl', 
        'fr' => 'fr', 
        'ro' => 'ro', 
        'de' => 'de', 
        'hu' => 'hu', 
        'si' => 'si', 
        'hr' => 'hr', 
        'no' => 'no', 
        'sv' => 'sv', 
    ];
    private static array $methodDescriptionLangs = [
        'cs' => 'cs', 
        'en' => 'en', 
        'pl' => 'pl', 
    ];
    
    public static function isCurrencyValid(string $currency): void
    {
        if(!(static::$currencies[$currency] ?? false)){
            throw ValidationException::createInvalidArgumentException(ValidationException::PAYMENT_CODE_KEY_CURENCY);
        }
    }

    public static function isCountryValid(string &$country): void
    {
        if (!$country) {
            $country = self::DEFAULT_COUNTRY;
            return;
        }

        if(!(static::$countries[$country] ?? false)){
            throw ValidationException::createInvalidArgumentException(ValidationException::PAYMENT_CODE_KEY_COUNTRY);
        }
    }

    public static function isPriceValid(string $currency, float $price): void
    {
        if ($price < static::$minPrices[$currency]) {
            throw ValidationException::createInvalidArgumentException(ValidationException::PAYMENT_CODE_KEY_PRICE);
        }
    }

    public static function isLabelValid(string $label): void
    {
        $len = Strings::length($label);

        if (empty($label) || $len > self::MAX_LABEL_LENGHT) {
            throw ValidationException::createInvalidArgumentException(ValidationException::PAYMENT_CODE_KEY_LABEL);
        }
    }

    //is method Valid

    public static function isEmailValid(string $email): void
    {
        if (!Validators::isEmail($email)) {
            throw ValidationException::createInvalidArgumentException(ValidationException::PAYMENT_CODE_KEY_EMAIL);
        }
    }

    public static function isPhoneValid(string $phone, string $continent = 'Europe', string $country = 'CZ'): void
    {
        if (!SnugValidators::isMobileValid($phone, $continent, $country)) {
            throw ValidationException::createInvalidArgumentException(ValidationException::PAYMENT_CODE_KEY_PHONE);
        }
    }

    public static function isLangValid(string &$lang): void
    {
        if (!$lang) {
            $lang = self::DEFAULT_LANG;
            return;
        }

        if(!(static::$langs[$lang] ?? false)){
            throw ValidationException::createInvalidArgumentException(ValidationException::PAYMENT_CODE_KEY_LANG);
        }
    }

    public static function isMethodDescriptionLang(string &$methodDescriptionLang): void
    {
        if (!$methodDescriptionLang) {
            $methodDescriptionLang = self::DEFAULT_METHOD_DESCRIPTION_LANG;
            return;
        }

        if(!(static::$methodDescriptionLangs[$methodDescriptionLang] ?? false)){
            throw ValidationException::createInvalidArgumentException(ValidationException::PAYMENT_CODE_KEY_METHOD_DESCRIPTION_LANG);
        }
    }

    public static function isEetDataValid(string $eetData): void
    {
        try {
            Json::decode($eetData);
        } catch (JsonException $e) {
            throw ValidationException::createInvalidArgumentException(ValidationException::PAYMENT_CODE_KEY_EET_DATA, $e);
        }
    }

    public static function isApplePayPayloadValid(string $applePayPayload): void
    {
        if(!Strings::match($applePayPayload, self::BASE64_REGEX)) {
            throw ValidationException::createInvalidArgumentException(ValidationException::PAYMENT_CODE_KEY_APPLE_PAY_PAYLOAD);
        }
    }
    
    public static function isMethodValid(
        string $method, 
        AllowedMethodsProvider $allowedMethodsProvider,
    ): void
    {
        $allMethods = $allowedMethodsProvider
                ->getAllowedMethods();
        
        $methodsToRegex = 'BANK_ALL)|(ALL';
        
        foreach ($allMethods as $methodArray) {
            $methodsToRegex .= ')|(' . $methodArray['id'];
        }
        
        $regex = sprintf(self::VALIDATE_METHOD_REGEX_BASE, $methodsToRegex);
        
        if (!Strings::match($method, $regex)) {
            throw ValidationException::createInvalidArgumentException(ValidationException::PAYMENT_CODE_KEY_METHOD);
        }
    }
}
