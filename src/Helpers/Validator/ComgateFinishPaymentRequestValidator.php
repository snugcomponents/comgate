<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate\Helpers\Validator;

use Nette\StaticClass;
use Snugcomponents\Comgate\Exceptions\ComgateFinishPaymentRequestException;
use Nette\Utils\Json;
use Snugcomponents\Comgate\Payment;
use Nette\Http\Helpers;
use Snugcomponents\Comgate\Providers\AllowedIpMasksProviderFactory;
use Nette\Utils\Strings;

class ComgateFinishPaymentRequestValidator
{
    use StaticClass;
    
    private const 
        STATUS_CASES = [
            'PAID' => 'PAID',
            'CANCELLED' => 'CANCELLED',
            'AUTHORIZED' => 'AUTHORIZED',
        ],
        PAYMENT_INFO = [
            'price' => ComgateFinishPaymentRequestException::CODE_KEY_BAD_PAYMENT_DATA_PRICE,
            'test' => ComgateFinishPaymentRequestException::CODE_KEY_BAD_PAYMENT_DATA_TEST,
            'refId' => ComgateFinishPaymentRequestException::CODE_KEY_BAD_PAYMENT_DATA_REF_ID,
            'curr' => ComgateFinishPaymentRequestException::CODE_KEY_BAD_PAYMENT_DATA_CURR,
            'label' => ComgateFinishPaymentRequestException::CODE_KEY_BAD_PAYMENT_DATA_LABEL,
            'email' => ComgateFinishPaymentRequestException::CODE_KEY_BAD_PAYMENT_DATA_EMAIL,
        ],
        PAYMENT_INFO_UNREQUIRED = [
            'account' => ComgateFinishPaymentRequestException::CODE_KEY_BAD_PAYMENT_DATA_ACCOUNT,
            'phone' => ComgateFinishPaymentRequestException::CODE_KEY_BAD_PAYMENT_DATA_PHONE,
            'name' => ComgateFinishPaymentRequestException::CODE_KEY_BAD_PAYMENT_DATA_NAME,
        ],
        PAYMENT_INFO_UNREQUIRED_STRINGS = [//jedna se o retezce, ktere posila comgate jako identifikaci platce
            'payerId' => ComgateFinishPaymentRequestException::CODE_KEY_BAD_PAYMENT_DATA_PAYER_ID,
            'payerName' => ComgateFinishPaymentRequestException::CODE_KEY_BAD_PAYMENT_DATA_PAYER_NAME,
            'payerAcc' => ComgateFinishPaymentRequestException::CODE_KEY_BAD_PAYMENT_DATA_PAYER_ACC,
        ];
    
    public static function isIpValid(
        string $ip,
        AllowedIpMasksProviderFactory $allowedIpMasksProviderFactory,
    ): void
    {
        $masks = $allowedIpMasksProviderFactory->create()->getAllowedIpMasks();
        
        foreach ($masks as $mask) {
            if(Helpers::ipMatch($ip, $mask)){
                return;
            }
        }
        
        throw ComgateFinishPaymentRequestException::createInvalidArgumentException(ComgateFinishPaymentRequestException::CODE_KEY_BAD_IP);
    }
    
    public static function isStatusValid(string $status): void
    {
        if (!isset(static::STATUS_CASES[$status])) {
            throw ComgateFinishPaymentRequestException::createInvalidArgumentException(ComgateFinishPaymentRequestException::CODE_KEY_BAD_STATUS);
        }
    }
    
    public static function isFeeValid(string $fee): void
    {
        if (!is_numeric($fee) && $fee != 'unknown') {
            throw ComgateFinishPaymentRequestException::createInvalidArgumentException(ComgateFinishPaymentRequestException::CODE_KEY_BAD_FEE);
        }
    }
    
    public static function isVSValid(string $vs): void
    {
        //if (!is_numeric($vs)) {
        if (!is_string($vs)) {
            throw ComgateFinishPaymentRequestException::createInvalidArgumentException(ComgateFinishPaymentRequestException::CODE_KEY_BAD_VS);
        }
    }
    
    public static function isSecretSame(string $original, string $returned): void
    {
        if ($original !== $returned) {
            throw ComgateFinishPaymentRequestException::createInvalidArgumentException(ComgateFinishPaymentRequestException::CODE_KEY_BAD_SECRET);
        }
    }
    
    public static function isMerchantSame(string $original, string $returned): void
    {
        if ($original !== $returned) {
            throw ComgateFinishPaymentRequestException::createInvalidArgumentException(ComgateFinishPaymentRequestException::CODE_KEY_BAD_MERCHANT);
        }
    }
    
    public static function isMethodValid(string $method, string $allowedMethods): void
    {
        if ($allowedMethods !== 'ALL' &&
            !(Strings::contains($allowedMethods, 'CARD_ALL') && Strings::startsWith($method, 'CARD_')) &&
            !(Strings::contains($allowedMethods, 'BANK_ALL') && Strings::startsWith($method, 'BANK_')) &&
            !(Strings::contains($allowedMethods, 'LATER_ALL') && Strings::startsWith($method, 'LATER_')) &&
            !Strings::contains($allowedMethods, $method)) {
            throw ComgateFinishPaymentRequestException::createInvalidArgumentException(ComgateFinishPaymentRequestException::CODE_KEY_BAD_PAYMENT_DATA_METHOD);
        }
    }
    
    public static function isPaymentInfoValid(
        array $data,
        Payment $payment,
    ): void 
    {
        foreach (static::PAYMENT_INFO as $key => $value) {
            if ($data[$key] != $payment->{$key}) {
                throw ComgateFinishPaymentRequestException::createInvalidArgumentException($value);
            }
        }
        
        foreach (static::PAYMENT_INFO_UNREQUIRED as $key => $value) {
            if (isset($data[$key]) && $data[$key] != $payment->{$key}) {
                throw ComgateFinishPaymentRequestException::createInvalidArgumentException($value);
            }
        }
        
        foreach (static::PAYMENT_INFO_UNREQUIRED_STRINGS as $key => $value) {
            if (isset($data[$key]) && !is_string($data[$key])) {
                throw ComgateFinishPaymentRequestException::createInvalidArgumentException($value);
            }
        }
        
        if (isset($data['eetData'])) {
            try {
                Json::decode($data['eetData']);
            } catch (Exception $ex) {
                throw ComgateFinishPaymentRequestException::createInvalidArgumentException(ComgateFinishPaymentRequestException::CODE_KEY_BAD_PAYMENT_DATA_EET_DATA, $ex);
            }
        }
        
        self::isMethodValid($data['method'], $payment->method);
    }
}
