<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate\Helpers;

use Snugcomponents\Comgate\Providers\PaymentConfirmRequestDataProvider;

interface ComgatePaymentConfirm
{
    public function confirm(PaymentConfirmRequestDataProvider $paymentConfirmRequestDataProvider): void;
}
