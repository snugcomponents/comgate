<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate\Helpers;

interface Logger
{
    public function log(mixed $data): void;
}
