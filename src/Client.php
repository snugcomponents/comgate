<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate;

use Snugcomponents\Comgate\Providers\CredentialsProvider;
use Snugcomponents\Comgate\Providers\AllowedMethodsProvider;
use Snugcomponents\Comgate\Providers\PaymentResponseManipulator;
use Snugcomponents\Comgate\Providers\RecurringPaymentResponseManipulator;
use Snugcomponents\Utils\Curl\ClassicRequest;
use Snugcomponents\Utils\Curl;
use Snugcomponents\Comgate\Helpers\Logger;
use Nette\Utils\Json;
use Nette\SmartObject;
use Nette\Utils\Strings;

class Client
{
    use SmartObject;
    
        private const
            URL_CREATE = 'https://payments.comgate.cz/v1.0/create',
            URL_RECURRING = 'https://payments.comgate.cz/v1.0/recurring',
            URL_METHODS = 'https://payments.comgate.cz/v1.0/methods',
            URL_IP = 'https://www.cloudflare.com/ips-v4',
            COMGATE_IP_MASK = '89.185.236.55/32',
            TYPE = 'json',
            BOOLEAN_TO_STRING = [
                true => 'true',
                false => 'false',
            ];
        
	public function __construct(
		private CredentialsProvider $credentialsProvider,
        private PaymentResponseManipulator $paymentResponseManipulator,
        private ?RecurringPaymentResponseManipulator $recurringPaymentResponseManipulator,
        private ?Logger $logger,
	) { }

    public function createPayment(Payment $payment): string
    {
        $request = $this->createPaymentRequest($payment);

        $resp = Curl::create()
            ->exec($request);

        $data = [];

        parse_str($resp, $data);

        $data['code'] = (int) $data['code'];

        if($data['code'] != 0) {
            $this->logger?->log($data);
            throw new Exception($data['message'], $data['code']);
        }

        $response = PaymentResponse::create(
            $payment,
            $data['transId'] ?? null,
            $data['redirect'] ?? null,
            $data['applepay'] ?? null,
            $data['applepayMessage'] ?? null,
        );

        $this->paymentResponseManipulator->savePaymentResponse($response);

        return $response->getRedirect();
    }

    public function doRecurringPayment(RecurringPayment $recurringPayment): void
    {
        if (!$this->recurringPaymentResponseManipulator) {
            throw new Exception('You need to implement ' . RecurringPaymentResponseManipulator::class . ' interface', 9999);
        }

        $request = $this->createPaymentRequest($recurringPayment);

        $resp = Curl::create()
            ->exec($request);

        $data = [];

        parse_str($resp, $data);

        $data['code'] = (int) $data['code'];

        if($data['code'] != 0) {
            $this->logger?->log($data);
            throw new Exception($data['message'], $data['code']);
        }

        $response = RecurringPaymentResponse::create(
            $recurringPayment,
            $data['transId'] ?? null,
        );

        $this->recurringPaymentResponseManipulator->saveRecurringPaymentResponse($response);
    }
        
        public function getAllowedMethods(AllowedMethodsProvider $allowedMethodsProvider): array
        {
            $request = ClassicRequest::create(static::URL_METHODS)
                    ->setData('merchant', $this->credentialsProvider->getComgateShopConnectIdentifier())
                    ->setData('secret', $this->credentialsProvider->getComgatePassword())
                    ->setData('type', static::TYPE)
                    ->setData('lang', $allowedMethodsProvider->lang)
                    ->setData('curr', $allowedMethodsProvider->currency)
                    ->setData('country', $allowedMethodsProvider->country)
                    ->waitForResponse()
                    ->makePost();

            $json = Curl::create()
                    ->exec($request);

            $resp = Json::decode($json, Json::FORCE_ARRAY);
            
            if (isset($resp['error'])) {
                $this->logger?->log($resp['error']);
                throw new Exception($resp['error']['message'], $resp['error']['code']);
            }
            
            return $resp['methods'];
        }
        
        public function getAllowedIpMasks(): array
        {
            $request = ClassicRequest::create(static::URL_IP)
                    ->waitForResponse()
                    ->makeGet();

            $ipsInString = Curl::create()
                    ->exec($request);
            
            $ips = explode(PHP_EOL, Strings::normalizeNewLines($ipsInString));
            $ips[] = static::COMGATE_IP_MASK;
            
            return $ips;
        }

    private function createPaymentRequest(BasePayment $payment): ClassicRequest
    {
        $url = $payment instanceof Payment ? static::URL_CREATE : static::URL_RECURRING;

        $request = ClassicRequest::create($url)
            ->setData('merchant', $this->credentialsProvider->getComgateShopConnectIdentifier())
            ->waitForResponse()
            ->makePost();

        $password = $this->credentialsProvider->getComgatePassword();

        if (!is_null($password)) {
            $request->setData('secret', $password);
        }

        foreach ($payment as $key => $value) {
            if (is_null($value)) {
                continue;
            }

            if (is_bool($value)) {
                $value = static::BOOLEAN_TO_STRING[$value];
            }

            $request->setData($key, $value);
        }

        return $request;
    }
}