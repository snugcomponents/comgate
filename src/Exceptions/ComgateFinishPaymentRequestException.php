<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate\Exceptions;

use Snugcomponents\Comgate\Exception;
use Exception as BaseException;

class ComgateFinishPaymentRequestException extends Exception
{
    public const
        CODE_KEY_BAD_IP = 101,
        CODE_KEY_BAD_STATUS = 102,
        CODE_KEY_BAD_PAYMENT_DATA = 103,
        CODE_KEY_BAD_PAYMENT_DATA_PRICE = 1031,
        CODE_KEY_BAD_PAYMENT_DATA_TEST = 1032,
        CODE_KEY_BAD_PAYMENT_DATA_REF_ID = 1033,
        CODE_KEY_BAD_PAYMENT_DATA_NAME = 1034,
        CODE_KEY_BAD_PAYMENT_DATA_CURR = 1035,
        CODE_KEY_BAD_PAYMENT_DATA_LABEL = 1036,
        CODE_KEY_BAD_PAYMENT_DATA_EMAIL = 1037,
        CODE_KEY_BAD_PAYMENT_DATA_METHOD = 1038,
        CODE_KEY_BAD_PAYMENT_DATA_PAYER_ID = 1039,
        CODE_KEY_BAD_PAYMENT_DATA_PAYER_NAME = 1040,
        CODE_KEY_BAD_PAYMENT_DATA_PAYER_ACC = 1041,
        CODE_KEY_BAD_PAYMENT_DATA_EET_DATA = 1042,
        CODE_KEY_BAD_PAYMENT_DATA_ACCOUNT = 1043,
        CODE_KEY_BAD_PAYMENT_DATA_PHONE = 1044,
        CODE_KEY_BAD_FEE = 104,
        CODE_KEY_BAD_VS = 105,
        CODE_KEY_BAD_MERCHANT = 106,
        CODE_KEY_BAD_SECRET = 107,
        OWN_MESSAGES = [
            self::CODE_KEY_BAD_IP => 'Sorry, but your request came from wrong ip address.',
            self::CODE_KEY_BAD_PAYMENT_DATA => 'Sorry, but your request contains bad payment data.',
        ],
        INVALID_ARGUMENT_CODES = [
            self::CODE_KEY_BAD_STATUS => 'status',
            self::CODE_KEY_BAD_PAYMENT_DATA_PRICE => 'price',
            self::CODE_KEY_BAD_PAYMENT_DATA_TEST => 'test',
            self::CODE_KEY_BAD_PAYMENT_DATA_REF_ID => 'refId',
            self::CODE_KEY_BAD_PAYMENT_DATA_NAME => 'name',
            self::CODE_KEY_BAD_PAYMENT_DATA_CURR => 'curr',
            self::CODE_KEY_BAD_PAYMENT_DATA_LABEL => 'label',
            self::CODE_KEY_BAD_PAYMENT_DATA_EMAIL => 'email',
            self::CODE_KEY_BAD_PAYMENT_DATA_METHOD => 'method',
            self::CODE_KEY_BAD_PAYMENT_DATA_PAYER_ID => 'payerId',
            self::CODE_KEY_BAD_PAYMENT_DATA_PAYER_NAME => 'payerName',
            self::CODE_KEY_BAD_PAYMENT_DATA_PAYER_ACC => 'payerAcc',
            self::CODE_KEY_BAD_PAYMENT_DATA_EET_DATA => 'eetData',
            self::CODE_KEY_BAD_PAYMENT_DATA_ACCOUNT => 'account',
            self::CODE_KEY_BAD_PAYMENT_DATA_PHONE => 'phone',
            self::CODE_KEY_BAD_FEE => 'fee',
            self::CODE_KEY_BAD_VS => 'vs',
            self::CODE_KEY_BAD_MERCHANT => 'merchant',
            self::CODE_KEY_BAD_SECRET => 'secret',
        ],
        INVALID_ARGUMENT_MESSAGE = 'Sorry, but parameter "%s" in your request is invalid.';
        
        public static function createInvalidArgumentException(int $codeKey, BaseException $previous = null): static
        {
            $message = static::OWN_MESSAGES[$codeKey] ??  sprintf(static::INVALID_ARGUMENT_MESSAGE, static::INVALID_ARGUMENT_CODES[$codeKey]);
            
            return new static(
                    $message, 
                    $codeKey,
                    $previous
                );
        }
}
