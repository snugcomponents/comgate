<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate\Exceptions;

use Snugcomponents\Comgate\Exception;
use Exception as BaseException;

class ValidationException extends Exception
{
    public const
        PAYMENT_CODE_KEY_CURENCY = 31,
        PAYMENT_CODE_KEY_COUNTRY = 32,
        PAYMENT_CODE_KEY_PRICE = 33,
        PAYMENT_CODE_KEY_LABEL = 34,
        PAYMENT_CODE_KEY_EMAIL = 35,
        PAYMENT_CODE_KEY_PHONE = 36,
        PAYMENT_CODE_KEY_LANG = 37,
        PAYMENT_CODE_KEY_EET_DATA = 38, 
        PAYMENT_CODE_KEY_APPLE_PAY_PAYLOAD = 39,
        PAYMENT_CODE_KEY_METHOD_DESCRIPTION_LANG = 40,
        PAYMENT_CODE_KEY_METHOD = 41,
        PAYMENT_INVALID_ARGUMENT_CODES = [
            self::PAYMENT_CODE_KEY_CURENCY => 'currency',
            self::PAYMENT_CODE_KEY_COUNTRY => 'country',
            self::PAYMENT_CODE_KEY_PRICE => 'price',
            self::PAYMENT_CODE_KEY_LABEL => 'label',
            self::PAYMENT_CODE_KEY_EMAIL => 'email',
            self::PAYMENT_CODE_KEY_PHONE => 'phone',
            self::PAYMENT_CODE_KEY_LANG => 'lang',
            self::PAYMENT_CODE_KEY_EET_DATA => 'eetData',
            self::PAYMENT_CODE_KEY_APPLE_PAY_PAYLOAD => 'applePayPayload',
            self::PAYMENT_CODE_KEY_METHOD_DESCRIPTION_LANG => 'methodDescriptionLang',
            self::PAYMENT_CODE_KEY_METHOD => 'method',
        ],
        PAYMENT_INVALID_ARGUMENT_MESSAGE = 'Sorry, but field "%s" is invalid.';
        
        public static function createInvalidArgumentException(int $codeKey, BaseException $previous = null): static
        {
            return new static(
                    sprintf(static::PAYMENT_INVALID_ARGUMENT_MESSAGE, static::PAYMENT_INVALID_ARGUMENT_CODES[$codeKey]), 
                    $codeKey,
                    $previous
                );
        }
}
