<?php declare(strict_types = 1);

namespace Snugcomponents\Comgate;

use Nette\DI\CompilerExtension;
use Nette\DI\Helpers;
use Nette\Schema\Expect;
use Nette\Schema\Schema;

class SnugcomponentsComgateExtension extends CompilerExtension
{

	public function getConfigSchema(): Schema
	{
		return Expect::structure([
			'eshopIdentifier' => Expect::string()->required(),
            'password' => Expect::string()->required(),
            'cacheTime' => Expect::string()->default('1 hour'),
		])->castTo('array');
	}

	public function loadConfiguration(): void
	{
		$this->compiler->loadDefinitionsFromConfig(
			Helpers::expand(
				$this->loadFromFile(__DIR__ . '/config/common.neon')['services'],
				(array) $this->config,
			),
		);
	}

}
