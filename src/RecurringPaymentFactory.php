<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate;

interface RecurringPaymentFactory
{
    public function create(
        int $price,
        string $curr,
        string $label,
        string $refId,
        string $email,
        bool $prepareOnly,

        string $initRecurringId,

        ?bool $test = null,
        ?string $country = null,
        ?string $account = null,
        ?string $phone = null,
        ?string $name = null,
        ?bool $eetReport = null,
        null|array|string $eetData = null,
    ): RecurringPayment;
}
