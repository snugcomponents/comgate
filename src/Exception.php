<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate;

use Exception as BaseException;

class Exception extends BaseException
{
	public const
		PAYMENT_EDIT_FIELD_CODE = 1,
		PAYMENT_EDIT_FIELD_MESSAGE = 'Sorry, but payment cannot be changed. Please make a new one.',
		PAYMENT_DELETE_FIELD_CODE = 2,
		PAYMENT_DELETE_FIELD_MESSAGE = 'Sorry, but payment properties cannot be deleted.';
}
