<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate\Providers;

use Snugcomponents\Comgate\Providers\AllowedIpMasksProvider;

interface AllowedIpMasksProviderFactory
{
    public function create(): AllowedIpMasksProvider;
}
