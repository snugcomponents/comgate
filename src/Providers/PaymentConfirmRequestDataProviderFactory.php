<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate\Providers;

use Snugcomponents\Comgate\Providers\PaymentConfirmRequestDataProvider;

interface PaymentConfirmRequestDataProviderFactory
{
    public function create(): PaymentConfirmRequestDataProvider;
}
