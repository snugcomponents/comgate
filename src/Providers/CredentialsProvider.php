<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate\Providers;

use Nette\SmartObject;

class CredentialsProvider
{
    use SmartObject;
    
    public function __construct(
        private string $eshopIdentifier,
        private string $password,
    ) { }

    public function getComgateShopConnectIdentifier(): string { return $this->eshopIdentifier; }
	public function getComgatePassword(): ?string { return $this->password; }
}
