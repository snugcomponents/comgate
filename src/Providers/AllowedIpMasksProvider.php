<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate\Providers;

use Snugcomponents\Comgate\Client;
use Nette\Caching\Storage;
use Nette\Caching\Cache;
use Closure;
use Nette\SmartObject;


/**
 * Class AllowedIpMasksProvider
 *
 * @package Snugcomponents\Comgate
 */
class AllowedIpMasksProvider
{
    use SmartObject;
    
    private Cache $cache;
    
    public function __construct(
        Storage $storage,
        private Client $client,
        private string $cacheTime,
    ) {
        $this->cache = new Cache($storage, 'SnugcomponentsComgateProvidersAllowedIpMasksProvider');
    }
    
    public function getAllowedIpMasks(): array
    {
        $cacheKey = 'ipmasks';

        $allowedIpMasks = $this->cache->load($cacheKey, Closure::fromCallable([$this, 'callClient']));

        return $allowedIpMasks;
    }
    
    private function callClient(&$dependencies): array
    {
        $dependencies[Cache::EXPIRATION] = $this->cacheTime;
        return $this->client->getAllowedIpMasks();
    }
}

