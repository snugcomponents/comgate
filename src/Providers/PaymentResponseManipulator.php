<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate\Providers;

use Snugcomponents\Comgate\PaymentResponse;

interface PaymentResponseManipulator
{
    public function savePaymentResponse(PaymentResponse $paymentResponse): void;
    public function getPaymentResponse(string $transId): PaymentResponse;
}

