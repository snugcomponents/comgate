<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate\Providers;

use Nette\Http\Request;
use Snugcomponents\Comgate\Providers\AllowedIpMasksProviderFactory;
use Snugcomponents\Comgate\Providers\PaymentResponseManipulator;
use Snugcomponents\Comgate\Helpers\Validator\ComgateFinishPaymentRequestValidator;
use Snugcomponents\Comgate\Providers\CredentialsProvider;
use Tracy\Debugger;

/**
 * @property-read bool $test
 * @property-read int $price
 * @property-read string $curr
 * @property-read string $label
 * @property-read string $refId
 * @property-read ?string $payerId
 * @property-read ?string $payerName
 * @property-read ?string $payerAcc
 * @property-read ?string $method
 * @property-read ?string $account
 * @property-read string $email
 * @property-read ?string $phone
 * @property-read ?string $name
 * @property-read string $transId
 * @property-read ?array $eetData
 * @property-read ?string $vs
 * @property-read ?string $status
 * @property-read ?string $fee
 * @property-read bool $recurring
 */
class PaymentConfirmRequestDataProvider
{
    public function __construct(
        AllowedIpMasksProviderFactory $allowedIpMasksProviderFactory,
        CredentialsProvider $credentialsProvider,
        PaymentResponseManipulator $paymentResponseManipulator,
        Request $request,
    ) {
        $ip = $request->getRemoteAddress();
        ComgateFinishPaymentRequestValidator::isIpValid($ip, $allowedIpMasksProviderFactory);
        
        $postData = $request->getPost();
        
        ComgateFinishPaymentRequestValidator::isMerchantSame($credentialsProvider->getComgateShopConnectIdentifier(), $postData['merchant']);
        ComgateFinishPaymentRequestValidator::isSecretSame($credentialsProvider->getComgatePassword(), $postData['secret']);
        
        $transId = $postData['transId'];
        $paymentResponse = $paymentResponseManipulator->getPaymentResponse($transId);

        ComgateFinishPaymentRequestValidator::isPaymentInfoValid($postData, $paymentResponse->getPayment());
        
        $status = $postData['status'];
        ComgateFinishPaymentRequestValidator::isStatusValid($status);
        
        $fee = $postData['fee'];
        ComgateFinishPaymentRequestValidator::isFeeValid($fee);
        
        $vs = $postData['vs'];
        ComgateFinishPaymentRequestValidator::isVSValid($vs);
        
        $this->test = (bool) $postData['test'];
        $this->price = (int) $postData['price'];
        $this->curr = $postData['curr'];
        $this->label = $postData['label'];
        $this->refId = $postData['refId'];
        $this->email = $postData['email'];
        $this->transId = $postData['transId'];
        $this->payerId = $postData['payerId'] ?? null;
        $this->payerAcc = $postData['payerAcc'] ?? null;
        $this->payerName = $postData['payerName'] ?? null;
        $this->method = $postData['method'] ?? null;
        $this->account = $postData['account'] ?? null;
        $this->phone = $postData['phone'] ?? null;
        $this->name = $postData['name'] ?? null;
        $this->eetData = $postData['eetData'] ?? null;
        $this->vs = $postData['vs'] ?? null;
        $this->status = $postData['status'] ?? null;
        $this->fee = $postData['fee'] ?? null;
        $this->recurring = $paymentResponse->getPayment()->initRecurring ?? false;
    }

    /****************************** GETTERS ******************************a*j*/
    
    public function isTest(): bool { return $this->test; }
    public function getPrice(): int { return $this->price; }
    public function getCurr(): string { return $this->curr; }
    public function getLabel(): string { return $this->label; }
    public function getRefId(): string { return $this->refId; }
    public function getPayerName(): string { return $this->payerName; }
    public function getPayerAcc(): string { return $this->payerAcc; }
    public function getPayerId(): string { return $this->payerId; }
    public function getMethod(): ?string { return $this->method; }
    public function getAccount(): ?string { return $this->account; }
    public function getEmail(): string { return $this->email; }
    public function getPhone(): ?string { return $this->phone; }
    public function getName(): ?string { return $this->name; }
    public function getTransId(): string { return $this->transId; }
    public function getEetData(): ?array { return $this->eetData; }
    public function getVs(): ?string { return $this->vs; }
    public function getStatus(): ?string { return $this->status; }
    public function getFee(): ?string { return $this->fee; }
    public function isRecurring(): bool { return $this->recurring; }
}
