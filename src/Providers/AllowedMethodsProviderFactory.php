<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate\Providers;

use Snugcomponents\Comgate\Providers\AllowedMethodsProvider;

interface AllowedMethodsProviderFactory
{
    public function create(
        string $lang,
        string $currency,
        string $country,
    ): AllowedMethodsProvider;
}
