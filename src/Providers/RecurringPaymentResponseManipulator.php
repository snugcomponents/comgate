<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate\Providers;

use Snugcomponents\Comgate\RecurringPaymentResponse;

interface RecurringPaymentResponseManipulator
{
    public function saveRecurringPaymentResponse(RecurringPaymentResponse $recurringPaymentResponse): void;
}

