<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate\Providers;

use Snugcomponents\Comgate\Helpers\Validator\PaymentValidator;
use Snugcomponents\Comgate\Client;
use Nette\Caching\Storage;
use Nette\Caching\Cache;
use Closure;
use Nette\SmartObject;

/**
 * @property-read string $country
 * @property-read string $currency
 * @property-read string $lang
 */
class AllowedMethodsProvider
{
    use SmartObject;
    
    private Cache $cache;
    
    public function __construct(
        Storage $storage,
        private Client $client,
        private string $cacheTime,
        private string $lang = 'cs',
        private string $currency = 'CZK',
        private string $country = 'CZ',
    ) {
        $this->cache = new Cache($storage, 'SnugcomponentsComgateProvidersAllowedMethodsProvider');
        PaymentValidator::isMethodDescriptionLang($this->lang);
        PaymentValidator::isCurrencyValid($this->currency);
        PaymentValidator::isCountryValid($this->country);
    }
    
    public function getAllowedMethods(): array
    {
        $cacheKey = md5($this->lang . $this->currency . $this->country);

        $allowedMethods = $this->cache->load($cacheKey, Closure::fromCallable([$this, 'callClient']));

        return $allowedMethods;
    }
    
    private function callClient(&$dependencies): array
    {
        $dependencies[Cache::EXPIRATION] = $this->cacheTime;
        return $this->client->getAllowedMethods($this);
    }
    
    
    public function getCurrency(): string {	return $this->currency; }
    public function getLang(): string {	return $this->lang; }
    public function getCountry(): string {	return $this->country; }
}

