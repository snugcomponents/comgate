<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate;

use ArrayAccess;
use Iterator;
use Nette\SmartObject;
use Snugcomponents\Comgate\Helpers\Validator\PaymentValidator;
use ReflectionClass;
use ReflectionProperty;

/**
 * Class Payment for details go for Comgate documentation
 * @see https://help.comgate.cz/docs/en/protocol-api-en#creating-a-payment
 * @see https://help.comgate.cz/docs/api-protokol#zalo%C5%BEen%C3%AD-platby
 *
 * @property-read ?bool $test
 * @property-read ?string $country
 * @property-read int $price
 * @property-read string $curr
 * @property-read string $label
 * @property-read string $refId
 * @property-read ?string $account
 * @property-read string $email
 * @property-read ?string $phone
 * @property-read ?string $name
 * @property-read bool $prepareOnly
 * @property-read ?bool $eetReport
 * @property-read ?array|string $eetData
 */
abstract class BasePayment implements ArrayAccess, Iterator
{
    use SmartObject;

    private array $elements = [];

    public function __construct(
        protected int $price,
        protected string $curr,
        protected string $label,
        protected string $refId,
        protected string $email,
        protected bool $prepareOnly,

        protected ?bool $test = null,
        protected ?string $country = null,
        protected ?string $account = null,
        protected ?string $phone = null,
        protected ?string $name = null,
        protected ?bool $eetReport = null,
        protected null|array|string $eetData = null,
    ) {
        PaymentValidator::isCurrencyValid($this->curr);
        PaymentValidator::isPriceValid($this->curr, $this->price);
        PaymentValidator::isLabelValid($this->label);
        PaymentValidator::isEmailValid($this->email);
        PaymentValidator::isCountryValid($this->country);
        if ($this->phone) {
            PaymentValidator::isPhoneValid(phone: $this->phone, country: $this->country);
        }
        if ($this->eetData) {
            PaymentValidator::isEetDataValid($this->eetData);
        }

        $reflect = new ReflectionClass($this);
        $props   = $reflect->getProperties(ReflectionProperty::IS_PROTECTED);

        $newPr = array_map(function (ReflectionProperty $reflectionProperty): string
        {
            return $reflectionProperty->name;
        }, $props);

        $this->elements = $newPr;
    }

    /****************************** ArrayAccess ******************************a*j*/

    public function offsetExists($offset): bool
    {
        return isset($this->$offset);
    }

    public function offsetGet($offset): mixed
    {
        return $this->$offset;
    }

    public function offsetSet($offset, $value): void
    {
        throw new Exception(Exception::PAYMENT_EDIT_FIELD_MESSAGE, Exception::PAYMENT_EDIT_FIELD_CODE);
    }

    public function offsetUnset($offset): void
    {
        throw new Exception(Exception::PAYMENT_DELETE_FIELD_MESSAGE, Exception::PAYMENT_DELETE_FIELD_CODE);
    }

    /****************************** GETTERS ******************************a*j*/

    public function getTest(): ?bool { return $this->test; }
    public function getCountry(): ?string {	return $this->country; }
    public function getPrice(): int { return $this->price; }
    public function getCurr(): string {	return $this->curr; }
    public function getLabel(): string { return $this->label; }
    public function getRefId(): string { return $this->refId; }
    public function getAccount(): ?string { return $this->account; }
    public function getEmail(): string { return $this->email; }
    public function getPhone(): ?string { return $this->phone; }
    public function getName(): ?string { return $this->name; }
    public function getPrepareOnly(): bool { return $this->prepareOnly; }
    public function getEetReport(): ?bool { return $this->eetReport; }
    public function getEetData(): array|string|null { return $this->eetData; }

    /****************************** Iterator ******************************m*b*/

    public function current(): mixed {
        if ($this->valid()) {
            return $this[$this->key()];
        }
        return false;
    }

    public function key(): mixed {
        return current($this->elements);
    }

    public function next(): void {
        next($this->elements);
    }

    public function rewind(): void {
        reset($this->elements);
    }

    public function valid(): bool {
        return (bool) $this->key();
    }
}
