<?php

declare(strict_types = 1);

namespace Snugcomponents\Comgate;

use Snugcomponents\Comgate\Payment;

/**
 * @property-read Payment $payment
 * @property-read ?string $transId
 * @property-read ?string $redirect
 * @property-read ?string $applepay
 * @property-read ?string $applepayMessage
 */
class PaymentResponse
{
    //vnitrni property a prejmenovat args
    public function __construct(
        private Payment $payment,
        private ?string $transId,
        private ?string $redirect,
        private ?string $applepay,
        private ?string $applepayMessage,
    ) { }
    
    public static function create(
           Payment $payment,
           ?string $transId,
           ?string $redirect,
           ?string $applepay,
           ?string $applepayMessage,
    ): static {
        return new static (
            $payment,
            $transId,
            $redirect,
            $applepay,
            $applepayMessage,
        );
    }

	/****************************** GETTERS ******************************m*b*/

	public function getPayment(): ?Payment { return $this->payment; }
	public function getTransId(): ?string { return $this->transId; }
	public function getRedirect(): ?string { return $this->redirect; }
	public function getApplepay(): ?string { return $this->applepay; }
	public function getApplepayMessage(): ?string {	return $this->applepayMessage; }
}
