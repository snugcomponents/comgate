# Quickstart
This extension provides basic interface for work with Comgate.

## Instalation

#### Download
The best way to install `snugcomponents/comgate`, is using Composer:
```sh
$ composer require snugcomponents/comgate
```

#### Registering and setting
You can enable and configure the extension using your neon config:
```sh
extensions:
	comgate: Snugcomponents\Comgate\SnugcomponentsComgateExtension

comgate:
	eshopIdentifier: %comgate.eshopIdentifier%
	password: %comgate.password%
	cacheTime: '2 hours' # unrequired
```

#### Simply use
At first you need to create payment and send it to Comgate. Comgate then rerurns URL for redirect, or for iframe:
```php
class BasePresenter extends Nette\Application\UI\Presenter
{
    #[Inject] public Snugcomponents\Comgate\PaymentFactory $paymentFactory; // Required for creating payment
    #[Inject] public Snugcomponents\Comgate\Client $comgateClient;          // Required for sending payment to Comgate

    public function actionComgateCreatePayment(): void
    {
        $payment = $this->paymentFactory->create(   // For explanation of arguments see Comgate official documentation.
            price: 5000,
            curr: 'CZK',
            label: 'Měsíční licence',
            refId: date('Ym') . 'U' . $this->user->getId(),
            email: $this->user->getIdentity()->getData()['email'],
            prepareOnly: true,
            method: 'ALL',
            initRecurring: false,
            test: !Debugger::$productionMode,
            country: 'CZ',
            //account: 'afes',
            phone: $this->user->getIdentity()->getData()['phone'] ?? null,
            name: 'Měsíční licence',
            eetReport: false,
            eetData: '{}',
            preauth: false,
            lang: 'cs',
            verification: false,
            embedded: false,
            applePayPayload: null,
        );

        $redirectUrl = $this->comgateClient->createPayment($payment);   // Creating payment. It sends payment to Comgate server and returns URL.

        $this->redirectUrl($redirectUrl); // Redirect to received URL.
    }
}
```
Then you need API endpoint, which is called by Comgate with payment result when payment is done:
```php
class BasePresenter extends Nette\Application\UI\Presenter
{
    #[Inject] public PaymentSaver $comgatePaymentSaver;     // Required for processing comgate request for our API endpoint
	
    public function actionPaymentConfirm(): void
    {
        try{
            $this->comgatePaymentSaver->confirm();  // This will do all the stuff.
        } catch (Exception $e) {
            // Please log the error and request. May be hack.
            $this->getHttpResponse()->setCode(IResponse::S400_BAD_REQUEST);
        }
        die();
    }
}
```
That is not all. You need to implement some interfaces. 
First of them is `Snugcomponents\Comgate\Providers\PaymentResponseManipulator`:
```php
interface PaymentResponseManipulator
{
    /**
     * Is responsible for saving response from Comgate. 
     * When you implement the creating payment and Comgate returns result,
     * then the result is forced to save via this method.
     * You need to save it somewhere save. 
     */
    public function savePaymentResponse(PaymentResponse $paymentResponse): void;
    /**
     * This method is called automatically, when is triggered confirmation of payment
     * by ComgatePaymentSaver. It is needed because of integrity of data.
     * @param string $transId This is unique identifier of PaymentResponse.
     */
    public function getPaymentResponse(string $transId): PaymentResponse;
}
```
and second is `Snugcomponents\Comgate\Providers\ComgatePaymentConfirm`:
```php
interface ComgatePaymentConfirm
{
    /**
     * After successfully confirmed integrity of data from ComgatePaymentSaver, this method is called.
     * Inside parameter you have all the data, which comgate provides and their integrity is confirmed.
     * You can do with that data whatever you want, 
     * but we are recommended to save information about that the payment was proceeded.
     */
    public function confirm(PaymentConfirmRequestDataProvider $paymentConfirmRequestDataProvider): void;
}
```

#### Recurring payments
When you need to use Recurring payments, then you need to implement another interface `Snugcomponents\Comgate\Providers\RecurringPaymentResponseManipulator`:
```php
interface RecurringPaymentResponseManipulator
{
    public function saveRecurringPaymentResponse(RecurringPaymentResponse $recurringPaymentResponse): void;
}
```
This interface is designed for saving information about recurring payment.
The following steps are required for recurring payment to work: (The individual steps are described in detail below)
1. User will create classic payment, but with `$initRecurring` set to true.
2. Application saves the `$transId` of this payment. It is CRITICAL that this value is never overwritten, unless you cancel the recurring payment. 
3. When you need to do recurring payment, then application or user will create instance of `Snugcomponents\Comgate\RecurringPayment`. This class requires parametr `$initRecurringId` which is the `$transId` from previous step.
4. The application must send this instance to the Comgate server using the `Snugcomponents\Comgate\Client::doRecurringPayment` method.
5. Previous step will automatically call `RecurringPaymentResponseManipulator::saveRecurringPaymentResponse` method.
6. If you want to cancel recurring payments, simply delete the `$transId` from step 2.

Steps 1 and 2 are described in the Simply use section and steps 5 and 6 are different for each implementation.
Step 3 and 4 should look like this:
```php
class BasePresenter extends Nette\Application\UI\Presenter
{
    #[Inject] public Snugcomponents\Comgate\RecurringPaymentFactory $recurringPaymentFactory; // Required for creating payment
    #[Inject] public Snugcomponents\Comgate\Client $comgateClient;          // Required for sending payment to Comgate

    public function actionComgateDoRecurringPayment(string $initRecurringId): void
    {
        $payment = $this->recurringPaymentFactory->create(   // This is step 3. For explanation of arguments see Comgate official documentation.
            price: 5000,
            curr: 'CZK',
            label: 'Měsíční licence',
            refId: date('Ym') . 'U' . $this->user->getId(),
            email: $this->user->getIdentity()->getData()['email'],
            prepareOnly: true,
            initRecurringId: $initRecurringId, // This variable can be loaded from database etc...
            test: !Debugger::$productionMode,
            country: 'CZ',
            //account: 'afes',
            phone: $this->user->getIdentity()->getData()['phone'] ?? null,
            name: 'Měsíční licence',
            eetReport: false,
            eetData: '{}',
        );

        $this->comgateClient->doRecurringPayment($payment);   // This is step 4. Do recurring payment. It sends payment to Comgate server and forces saving of response.
    }
}
```
In recurring payment there is no redirection, so there is no confirm endpoint.
The payment is made in the background, so the result of the payment is immediately forwarded in response from Comgate and forced save via step 5.


That's all. Enjoy.

## Conclusion
This extension requires PHP8.1 and Nette3.1, and it is property of SnugDesign © 2021
